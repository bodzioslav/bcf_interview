Exercice #3:

Let's assume that we have a repository with a simple landing page code (static site that consists of html, css and image files):

At first we are commiting changes to our git repository. After doing so our CI/CD server (eg. jenkins pipeline) pulls that code from git, runs linters, tests and so on...

If they pass then we can push our site to a production environment (for example by sending those files over FTP/SCP to a proper webroot directory). Deploy process won't happen in the case of failure.   
